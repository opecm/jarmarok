app.service('newsService', function(){

	var news = {};

	var service = {
		setNews: function(data){
			news = data;
		},
		getNews: function(){
			return news;
		}
	}

	return service;

})