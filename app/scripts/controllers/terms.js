'use strict';

/**
 * @ngdoc function
 * @name jarmarockApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the jarmarockApp
 */
app
  .controller('TermsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });