'use strict';

/**
 * @ngdoc function
 * @name jarmarockApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the jarmarockApp
 */
app
  .controller('MapCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    
 angular.element(document).ready( function() {
      function openNav() {
        document.getElementById("pushMenu").style.right = "0%";
        document.getElementById("content").style.marginright = "0";
        document.getElementById("content").style.transition = ".5s";
        var menu = document.getElementById('pushMenu');
        var container = document.getElementById('full-content');
        console.log(container);
        container.setAttribute('class', 'move-container');
      };

      function closeNav() {
        document.getElementById("pushMenu").style.right = "-101%";
        document.getElementById("pushMenu").style.marginRight = "-0";
        var containers = document.getElementById('full-content');
        console.log(containers);
        var menu = document.getElementById('pushMenu');
        containers.setAttribute('class', 'move-back-container');
      };

  

      var hamburger = document.getElementById('nav-icon4');
      hamburger.addEventListener('click', function (event) {
        event.preventDefault();
        if (document.getElementById("pushMenu").style.right == "-101%" || document.getElementById("pushMenu").style.right == '') {
          openNav();
          document.getElementById('nav-icon4').className += 'open';
        }
        else {
          closeNav();
          document.getElementById('nav-icon4').className = '';
        }
      });


      function initMap() {
        var myLatLng = {
            lat: 54.3646306
            , lng: 18.6485028
        , };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15
            , scrollwheel: false
            , center: myLatLng
            , styles: [{
                "featureType": "all"
                , "elementType": "labels.text.fill"
                , "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "featureType": "all"
                , "elementType": "labels.text.stroke"
                , "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "all"
                , "elementType": "labels.icon"
                , "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative"
                , "elementType": "geometry.fill"
                , "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative"
                , "elementType": "geometry.stroke"
                , "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }, {
                "featureType": "landscape"
                , "elementType": "geometry"
                , "stylers": [{
                    "lightness": 20
                }, {
                    "color": "#ececec"
                }]
            }, {
                "featureType": "landscape.man_made"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#f0f0ef"
                }]
            }, {
                "featureType": "landscape.man_made"
                , "elementType": "geometry.fill"
                , "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#f0f0ef"
                }]
            }, {
                "featureType": "landscape.man_made"
                , "elementType": "geometry.stroke"
                , "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#d4d4d4"
                }]
            }, {
                "featureType": "landscape.natural"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ececec"
                }]
            }, {
                "featureType": "poi"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi"
                , "elementType": "geometry"
                , "stylers": [{
                    "lightness": 21
                }, {
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi"
                , "elementType": "geometry.fill"
                , "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#d4d4d4"
                }]
            }, {
                "featureType": "poi"
                , "elementType": "labels.text.fill"
                , "stylers": [{
                    "color": "#303030"
                }]
            }, {
                "featureType": "poi"
                , "elementType": "labels.icon"
                , "stylers": [{
                    "saturation": "-100"
                }]
            }, {
                "featureType": "poi.attraction"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.business"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.government"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.medical"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.park"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.park"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.place_of_worship"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.school"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.school"
                , "elementType": "geometry.stroke"
                , "stylers": [{
                    "lightness": "-61"
                }, {
                    "gamma": "0.00"
                }, {
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi.sports_complex"
                , "elementType": "all"
                , "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "road.highway"
                , "elementType": "geometry.fill"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway"
                , "elementType": "geometry.stroke"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "transit"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "water"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#dadada"
                }, {
                    "lightness": 17
                }]
            }]
        });
        var marker = new google.maps.Marker({
            position: myLatLng
            , map: map
            , title: 'Hello World!'
            , icon: '/images/logo2.jpg'
        });
    }


  
    });

  });

