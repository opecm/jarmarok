'use strict';

/**
 * @ngdoc function
 * @name jarmarockApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jarmarockApp
 */
app
  .controller('MainCtrl', function () {
    window.onload = function() {
      function openNav() {
        document.getElementById("pushMenu").style.right = "0%";
        document.getElementById("content").style.marginright = "0";
        document.getElementById("content").style.transition = ".5s";
        var menu = document.getElementById('pushMenu');
        var container = document.getElementById('full-content');
        console.log(container);
        container.setAttribute('class', 'move-container');
      };

      function closeNav() {
        document.getElementById("pushMenu").style.right = "-101%";
        document.getElementById("pushMenu").style.marginRight = "-0";
        var containers = document.getElementById('full-content');
        console.log(containers);
        var menu = document.getElementById('pushMenu');
        containers.setAttribute('class', 'move-back-container');
      };

      var hamburger = document.getElementById('nav-icon4');
      hamburger.addEventListener('click', function (event) {
        event.preventDefault();
        if (document.getElementById("pushMenu").style.right == "-101%" || document.getElementById("pushMenu").style.right == '') {
          openNav();
          document.getElementById('nav-icon4').className += 'open';
        }
        else {
          closeNav();
          document.getElementById('nav-icon4').className = '';
        }
      });

    };

    function changeBackgroundOnMouse() {
      var backgroundImage = document.getElementById('main-image')
      var concertPart1 = document.getElementById('molodamuza');
      var concertPart2 = document.getElementById('legends');
      var concertPart3 = document.getElementById('hulanka');
      concertPart1.onmouseover = function () {
        backgroundImage.style.backgroundImage = "url('/images/molodamuza.jpg')";
      }
      concertPart2.onmouseover = function () {
        backgroundImage.style.backgroundImage = "url('/images/legends.jpg')";
      }
      concertPart3.onmouseover = function () {
        backgroundImage.style.backgroundImage = "url('/images/hulanka.jpg')";
      }
    }

    function scroller() {
      var windowWith = $(window).width();
      var scroll_start = 0;
      var startchange = $('#change-image');
      var startchange2 = $('#change-image2');
      var startchange3 = $('#change-image3');
      var startchange4 = $('#change-image4');
          var startchange5 = $('#change-image5');
      var offset = startchange.offset();
      var offset2 = startchange2.offset();
      var offset3 = startchange3.offset();
      var offset4 = startchange4.offset();
         var offset5 = startchange5.offset();
      scroll_start = $(this).scrollTop();
      if (windowWith > 500) {
        if (scroll_start > offset.top && scroll_start < offset2.top) {
          $('#main-image').css({
            'background': "url('/images/newphoto/back12.jpg') center center no-repeat"
            , 'background-size': 'cover'
            , 'height': '100vh'
            , 'transition': '0.3s'
       
//    ,'background-color': 'rgba(194, 26, 26, 0.48)'
//                  ,'background-blend-mode': 'color-dodge'  
          })
        }
        else if (scroll_start > offset4.top && scroll_start < offset.top) {
          $('#main-image').css({
            'background': "url('/images/back2.jpg') center center no-repeat "
            , 'transition': '0.3s'
            , 'background-size': 'cover'
            , 'height': '100vh'
//                  ,'background-color': 'rgba(194, 26, 26, 0.48)'
//                  ,'background-blend-mode': 'difference'  
          })
        }
           else if (scroll_start > offset5.top && scroll_start < offset4.top) {
          $('#main-image').css({
            'background': "url('/images/newphoto/back332.jpg') center center no-repeat "
            , 'transition': '0.3s'
            , 'background-size': 'cover'
            , 'height': '100vh'
          })
        }
          
            else if (scroll_start > offset2.top) {
          $('#main-image').css({
            'background': "url('/images/newphoto/back311.jpg') center center no-repeat "
            , 'transition': '0.3s'
            , 'background-size': 'cover'
            , 'height': '100vh'
          })
        }
        else {
          $('#main-image').css({
            'background': "url('/images/poster3.jpg') center center no-repeat"
            , 'background-size': 'cover'
            , 'height': '100vh'
            , 'transition': '0.3s'
          })
        }
      }

      else {  if (scroll_start > offset3.top && scroll_start < offset4.top) {
        $('#main-image').css({
          'background': "url('/images/back2.jpg') center center no-repeat"
          , 'background-size': 'cover'

          , 'transition': '0.1s'
          ,    'background-blend-mode': 'difference'
//                ,    'background-color': 'rgba(33, 175, 181, 0.43)'
//                  ,    'background-position-x': '-300px'
//   ,'background-color': 'rgba(42, 142, 187, 0.38)'
        })
      }
      else if (scroll_start > offset4.top) {
        $('#main-image').css({
          'background': "url('/images/back7.jpg') center center no-repeat "
          , 'transition': '0.3s'
          , 'background-size': 'cover'
          , 'height': '100vh'
          ,    'background-blend-mode': 'luminosity'
//                , 'background-color':' rgba(16, 13, 11, 0.36)'
        })
      }
      else {
        $('#main-image').css({
          'background': "url('/images/poster3.jpg') center center no-repeat"
          , 'background-size': 'cover'
          , 'height': '100vh'
          , 'transition': '0.1s'
        })
      }
      }

    }

    $(document).ready(function () {
      $(window).scroll(scroller);
      changeBackgroundOnMouse();
    });
  });
