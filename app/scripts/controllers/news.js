/**
 * Created by pp on 2016-10-17.
 */
'use strict';

/**
 * @ngdoc function
 * @name jarmarockApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the jarmarockApp
 */
app
  .controller('NewsCtrl', function (Facebook, $scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];



    Facebook.api('/312125182138194/posts?access_token=291288937551399|eb66d53f25a45e21e0d8d91959e76320', function (response) {
      $scope.news = response.data;
      for (var i = $scope.news.length - 1; i >= 0; i--) {
        if ($scope.news[i].message == undefined) {
          $scope.news.splice(i, 1);
          var str = $scope.news[i].message;
          str.replace(/(?:http?|ftp):\/\/[\n\S]+/g, '');
        } else if ($scope.news[i].type == 'video') {
          $scope.news.splice(i, 1);
        } else if ($scope.news[i].name == "Timeline Photos") {
          $scope.news[i].name = "Twój Stary";
        }
        $scope.news[i].message = $scope.news[i].message.replace(/(?:http?|ftp?|https):\/\/[\n\S]+/g, '');
      }


      angular.forEach($scope.news, function (news) {
        Facebook.api(news.object_id + '/picture?access_token=291288937551399|eb66d53f25a45e21e0d8d91959e76320', function (response) {
          var x = news.picture;
          var url = x.replace('w=130&h=130', 'w=360&h=360');
          if (news.object_id == undefined) {
            news.bigPhoto = url;
          }
          else {
            news.bigPhoto = response.data.url;
          }
        });
      });
      console.log($scope);
    });
 
  });