'use strict';

/**
 * @ngdoc function
 * @name jarmarockApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the jarmarockApp
 */
app
  .controller('BandsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    angular.element(document).ready( function() {
      function openNav() {
        document.getElementById("pushMenu").style.right = "0%";
        document.getElementById("content").style.marginright = "0";
        document.getElementById("content").style.transition = ".5s";
        var menu = document.getElementById('pushMenu');
        var container = document.getElementById('full-content');
        console.log(container);
        container.setAttribute('class', 'move-container');
      };

      function closeNav() {
        document.getElementById("pushMenu").style.right = "-101%";
        document.getElementById("pushMenu").style.marginRight = "-0";
        var containers = document.getElementById('full-content');
        console.log(containers);
        var menu = document.getElementById('pushMenu');
        containers.setAttribute('class', 'move-back-container');
      };

      function closeBandModal() {
    var close = document.querySelector('.band-description');
    close.style.display = 'none';
}

      var hamburger = document.getElementById('nav-icon4');
      hamburger.addEventListener('click', function (event) {
        event.preventDefault();
        if (document.getElementById("pushMenu").style.right == "-101%" || document.getElementById("pushMenu").style.right == '') {
          openNav();
          document.getElementById('nav-icon4').className += 'open';
        }
        else {
          closeNav();
          document.getElementById('nav-icon4').className = '';
        }
      });

      function updateGalerry(e, content) {
        var queryVal = 'li' + e.target.childNodes[0].getAttribute("href");
        console.log(queryVal);
        e.preventDefault(queryVal);
        for (var i = 0; i < listOfMasks.length; i++) {
          listOfMasks[i].classList.remove('mask5')
        }
        e.target.classList.add('mask5');
        content.querySelector('li.selected').classList.remove('selected');
        console.log(content.querySelector(queryVal));
        content.querySelector(queryVal).classList.add('selected');
      }
      var selectedBand = document.querySelector('.mask5');
      var bandInformation = document.getElementsByClassName('band-text')[0];
      var bandGalerry = document.querySelector('.band-galerry');
      var close = document.querySelector('.band-description');
      console.log(bandGalerry.children);
      console.log(selectedBand);
      var listOfMasks = document.getElementsByClassName('mask');
      console.log(listOfMasks);
      bandGalerry.addEventListener('click', function (event) {
        close.style.display = 'block';
        console.log('hello');
        //         alert(event.target.nodeName);
        if (event.target.classList.contains('mask')) {
          updateGalerry(event, bandInformation);
        }
        console.log(e.target.childNodes[0].getAttribute("href"));
      });


      var closeButton = document.getElementById("bands-front");
      var closer = document.getElementsByClassName('band-text')[0];
      closer.addEventListener('click', function () {
        closeBandModal();
      });
    });

  });
