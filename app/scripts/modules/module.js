define([
	'angular'
], function (angular) {
	'use strict';

	var module = angular.module('jarmarockApp.navbar', [
	]);

	module.config(function ($stateProvider) {

		$stateProvider
	        .state('navbar', {
	        	abstract: true,
	          	views: {
	            	'root': {
	              		templateUrl: 'app/views/footer.html',
	              		controller: function (currentApp, $state, $rootScope) {
	              			$rootScope.currentApp = currentApp;
			          		if (!currentApp) {
			          			$state.transitionTo('apps.list');
			          		}
			          	},
			          	resolve: {
			          		currentApp: function (authService) {
			          			return authService.getCurrentApp();
			          		}
			          	}
	            	}
	          	},
	          	authenticate: true
	        })
	     
	});

	return module;
});
