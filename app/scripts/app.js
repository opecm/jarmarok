'use strict';

/**
 * @ngdoc overview
 * @name jarmarockApp
 * @description
 * # jarmarockApp
 *
 * Main module of the application.
 */
var app = angular
  .module('jarmarockApp', [
    'ngAnimate',
    'ngAria',
    'ngRoute',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial',
    'facebook',
    'smoothScroll'


  ])
  .config(function(FacebookProvider) {
     FacebookProvider.init('291288937551399');
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/terms', {
        templateUrl: 'views/clubRules.html',
        controller: 'TermsCtrl',
        controllerAs: 'terms'
      })
      .when('/article/:articleId', {
        templateUrl: 'views/singleFb.html',
        controller: 'SingleFbCtrl',
        controllerAs: 'singleFb'
      })
      .when('/map', {
        templateUrl: 'views/clubMap.html',
        controller: 'MapCtrl',
        controllerAs: 'map'
      })
    .when('/bands', {
        templateUrl: 'views/bands.html',
        controller: 'BandsCtrl',
        controllerAs: 'bands'
      })
      .when('/news', {
        templateUrl:'views/news.html',
        controller: 'NewsCtrl',
        controllerAs: 'news'
      })
      .otherwise({
        redirectTo: '/'
      });
  });


